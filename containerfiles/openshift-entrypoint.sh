#!/bin/bash

if [[ -n ${DEBUG} ]];then
  set -x
fi

set -o errexit
#set -o nounset
set -o pipefail

if [[ -n ${DEBUG} ]];then
  echo "NO_UPSTREAM       :"${NO_UPSTREAM}
  echo "UPSTREAM          :"${UPSTREAM}
  echo "UPSTREAM_PORT     :"${UPSTREAM_PORT:-8888}
  echo "TZ                :"${TZ}
  echo "WHITELIST_FILE    :"${WHITELIST_FILE}
  echo "WHITELIST_DOMAINS :"${WHITELIST_DOMAINS}
  echo "FORCE             :"${FORCE}
  echo "DEFAULT_SITE      :"${DEFAULT_SITE}
  echo "CERT              :"${CERT}
  echo "CA                :"${CA}
  echo "DEBUG             :"${DEBUG}
  echo "CUSTOM_METHOD:    :"${CUSTOM_METHOD}
fi

function die() {
    echo "$*" 1>&2
    exit 1
}

if [[ -z "${WHITELIST_DOMAINS}" ]]; then
  die "Error a white list domains MUST be defined"
fi

IPADDR_REGEX="[[:xdigit:].:]*[.:][[:xdigit:].:]+"
OPT_CIDR_MASK_REGEX="(/[[:digit:]]+)?"
HOSTNAME_REGEX="[[:alnum:]][[:alnum:].-]+"
DOMAIN_REGEX="\*\.${HOSTNAME_REGEX}"

function generate_acls() {
  n=0
  saw_wildcard=

  for dest in $( echo ${WHITELIST_DOMAINS}| sed -e 's/;/ /g'); do
    if [[ "${dest}" =~ ^\w*$ || "${dest}" =~ ^# ]]; then
	    # comment or blank line
	    continue
	  fi
	  n=$(($n + 1))

    if [[ "${dest}" == "*" ]]; then
	    saw_wildcard=1
	    continue
    elif [[ -n "${saw_wildcard}" ]]; then
	    die "Wildcard must be last rule, if present"
    fi

    if [[ "${dest}" =~ ^! ]]; then
	    rule=deny
	    dest="${dest#!}"
    else
	    rule=allow
    fi

    echo ""
    if [[ "${dest}" =~ ^${IPADDR_REGEX}${OPT_CIDR_MASK_REGEX}$ ]]; then
	    echo acl dest${n} dst "${dest}"
	    echo http_access "${rule}" dest${n}
      if [[ -n ${CUST_METH} ]]; then
        echo http_access allow CUSTOM_METHOD safe_ports_http  dest${n}
      fi
	    echo http_access allow http    safe_ports_http  dest${n}
	    echo http_access allow CONNECT safe_ports_https dest${n}
    elif [[ "${dest}" =~ ^${DOMAIN_REGEX}$ ]]; then
	    echo acl dest${n} dstdomain "${dest#\*}"
	    echo http_access "${rule}" dest${n}
      if [[ -n ${CUST_METH} ]]; then
        echo http_access allow CUSTOM_METHOD safe_ports_http  dest${n}
      fi
	    echo http_access allow http    safe_ports_http  dest${n}
	    echo http_access allow CONNECT safe_ports_https dest${n}
    elif [[ "${dest}" =~ ^${HOSTNAME_REGEX}$ ]]; then
	    echo acl dest${n} dstdomain "${dest}"
      if [[ -n ${CUST_METH} ]]; then
        echo http_access allow CUSTOM_METHOD safe_ports_http  dest${n}
      fi
	    echo http_access allow http    safe_ports_http  dest${n}
	    echo http_access allow CONNECT safe_ports_https dest${n}
	    echo http_access "${rule}" dest${n}
    else
	    die "Bad destination '${dest}'"
    fi
  done

  echo ""
  if [[ -n "${saw_wildcard}" ]]; then
    echo "http_access allow all"
  else
    echo "http_access deny all"
  fi
}
# end function generate_acls


# cache peer only set if NO_UPSTREAM=false && UPSTREAM not empty
if [[ "${NO_UPSTREAM}" = "false" ]]; then
  if [[ -z "${UPSTREAM}" ]]; then
    die "Error the UPSTREAM MUST be defined"
  else
    SET_UPSTREAM=$(envsubst < /etc/squid/squid.upstream.template)
    export SET_UPSTREAM
  fi
fi

PORT_LINE="http_port 8080"
export PORT_LINE

# That must be the path where the secrets are mounted
# oc volumes dc/openpaas-squidext --add --name=rh-certs --secret-name=rh-certs \
# --mount-path=/data/secrets <= this one ;-)
if [[ -n "${CERT}" && -n ${DEFAULT_SITE} ]]; then

  if [ ! -d "${CERT}" ]; then
    die "${CERT} is not a directory"
  fi

  PORT_LINE="https_port 8080 accel cert=${CERT}/SSL_Certs cafile=${CERT}/SSL_CA name=rh-client-cert defaultsite=${DEFAULT_SITE} "
  export PORT_LINE

  SET_UPSTREAM=$(envsubst < /etc/squid/squid.origin-server.template)
  export SET_UPSTREAM
fi

if [[ -n ${CUSTOM_METHOD} ]]; then
  CUST_METH="acl CUSTOM_METHOD method "$(echo ${CUSTOM_METHOD}| sed -e 's/;/ /g')
else
  CUST_METH=""
fi

export CUST_METH

ACLS=$( generate_acls )

if [[ "${EGRESS_HTTP_PROXY_MODE:-}" == "unit-test" ]]; then
    die ${ACLS}
fi

export ACLS
envsubst < /etc/squid/squid.conf.template > /tmp/squid.conf

if [[ -n ${DEBUG} ]];then
  echo "Current used /tmp/squid.conf"
  echo "-------"
  cat /tmp/squid.conf
  echo "-------"
fi

exec /usr/sbin/squid -f /tmp/squid.conf -N
