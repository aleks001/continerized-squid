# Egress proxy

This repository builds and runs squid proxy as a egress proxy in a project.

https://github.com/openshift/origin/pull/13586/files

upcomming

https://github.com/openshift/origin/tree/master/images/egress


## dedicated project

If you want to offer this image from a dedicated project you will need to add the following permissions to the project


```
oadm policy add-cluster-role-to-group system:image-puller system:authenticated ...
```

For example in the `platform-images` project.

```
oadm policy add-cluster-role-to-group system:image-puller system:authenticated -n platform-images
```

# git fetch secret

You need to add the following secret to your project.

```
MY_PW=$(echo -n '<YOUR_PASSWORD>' |base64)
MY_USER=$(echo -n '<YOUR_GIT_UUSERNAME>' |base64)

oc process -f 01_git_pull_secret.yaml -v MY_PW=${MY_PW} \
    -v MY_USER=${MY_USER} \
    -v MY_SECRET=basicauth \
	| oc create -f -
```

# Imagestreams

```
oc create -f 01_is_openpaas-squid-accesslog.yaml
oc create -f 01_is_openpaas-squid-3-3.yaml
```

# Build

## HTTP / Squid

The template requires proxies!
The secret name must match from the abouve task **git fetch secret**

```
oc process -f 02_buildconfig_openpaas-squid-3-3.json \
    -v HTTP_PROXY=${http_proxy} \
    -v HTTPS_PROXY=${https_proxy} \
    -v GIT_URI=https://...squid_ext.git \
    -v SECRET_NAME=basicauth

```

if the output looks good you can pipe it to oc create.

```
oc process -f 01_buildconfig_openpaas-squid-3-3.json \
    -v HTTP_PROXY=${http_proxy} \
    -v HTTPS_PROXY=${https_proxy} \
    -v GIT_URI=https://...squid_ext.git \
    -v SECRET_NAME=basicauth \
    | oc create -f -
```

# Use as Template

A administator need to add the template into the openshift namespace


```
oc create -f 03_deploymentconfig_log-and-squid.yaml -n openshift
```

Now the customer can use this template as any other template in openshift

```
[X043693@ip-10-191-13-83 metadata]$ oc new-app openpaas-squid-3-3 -p UPSTREAM=false
--> Deploying template "openpaas-squid-3-3" in project "openshift" for "openpaas-squid-3-3"
     With parameters:
      UPSTREAM=false
      TZ=UTC
      WHITELIST_FILE=/tmp/default_whitelist.txt
      WHITELIST_DOMAINS=.github.com,.example.com
      FORCE=false
      TCPLOGGER_PORT=tcp://127.0.0.1:8514
      NO_UPSTREAM=true
      PROXY_NAMESPACE=platform-images
--> Creating resources ...
    service "openpaas-squid-3-3" created
    deploymentconfig "openpaas-squid-3-3" created
--> Success
    Run 'oc status' to view your app.
```

## use Red Hat server certificates

```
oc secret new rh-certs SSL_Certs=../../RH_squid_cert_key.pem SSL_CA=/etc/rhsm/ca/redhat-uep.pem
oc secrets link default rh-certs
oc env dc/openpaas-squidext CERT=/data/secrets
oc create configmap acl-file --from-file=example-acl-file.conf
oc volumes dc/openpaas-squidext --add --name=rh-certs --secret-name=rh-certs --mount-path=/data/secrets
oc volumes dc/openpaas-squidext --add --name=acl-file --configmap-name=acl-file --type=configmap --mount-path=/data/acls
```

# Deployment Config

## Just for information!

The normal http_proxy string looks like this

```
echo $http_proxy
http://internal-elbProxy-558275737.eu-central-1.elb.amazonaws.com:8888
```

With the ```echo $http_proxy | perl -F'/http:\/\/(.*):88/' -ane 'print $F[1]'``` you will get only the domain part which is needed by squid

For the right syntax of the whitelist domain please take a look into the doc of squid.

http://www.squid-cache.org/Versions/v3/3.3/cfgman/acl.html

please search for `dstdomain` on the site abouve

## create dc

The command below sets a upstream server. If you don't need a upstream simply don't set ```UPSTREAM``` in the ```oc process``` call.

The line below will setup a project based squid with a upstream configuration.

```
oc process -f 03_deploymentconfig_log-and-squid.yaml \
    -v WHITELIST_DOMAINS='.google.com;.google.sg' \
    -v WHITELIST_FILE='/tmp/my_WHITELIST_FILE.txt' \
    -v UPSTREAM=$(echo $http_proxy | perl -F'/http:\/\/(.*):88/' -ane 'print $F[1]') \
    -v NO_UPSTREAM=false
```

The line below will setup a project based squid without a upstream configuration.

```
oc process -f 03_deploymentconfig_log-and-squid.yaml \
    -v WHITELIST_DOMAINS='.google.com;.google.sg' \
    -v WHITELIST_FILE='/tmp/my_WHITELIST_FILE.txt' \
    -v UPSTREAM=$(echo $http_proxy | perl -F'/http:\/\/(.*):88/' -ane 'print $F[1]') \
    -v UPSTREAM_PORT=$(echo $http_proxy | perl -F'/http:\/\/.*:(\d+)/' -ane 'print $F[1]') \
    -v NO_UPSTREAM=true \
  | oc create -f -
```

if the output looks good you can pipe it to oc create.

```
<THE COMMAND ABOUVE>
	| oc create -f -
```

# deploy

```
oc deploy openpaas-squid-3-3 --latest

```

# Logs

To reach now the squid logs you must now execute this

```
oc logs -f -c openpaas-squid-3-3 <PODNAME>
oc logs -f -c openpaas-squid-accesslog <PODNAME>
```

for example

```
oc logs -f -c openpaas-squid-3-3 openpaas-squid-3-3-3-x0q7a
oc logs -f -c openpaas-squid-accesslog openpaas-squid-3-3-3-x0q7a
```

# Add whitelist domain

to edit the whitelisted domains you need to change the variable `WHITELIST_DOMAINS`.

The sntax for this whitelisting values is described on this page at the keyword `dstdomain`

http://www.squid-cache.org/Versions/v3/3.3/cfgman/acl.html

```
oc env dc/openpaas-squid-3-3 WHITELIST_DOMAINS=".google.com;.google.sg;www.google.de"
```

# Set PROXY for build config

```
oc env bc/ruby-hello-world \
    HTTP_PROXY="http://openpaas-squid-3-3:8080/" \
    HTTPS_PROXY="http://openpaas-squid-3-3:8080/" \
    http_proxy="http://openpaas-squid-3-3:8080/" \
    https_proxy="http://openpaas-squid-3-3:8080/"
```
# trouble shooting

## docker

Run the following command to build and run this

```
docker build --rm -t openpaas-squid_ext .
docker run --rm -it --entrypoint /bin/bash -v /root/openpaas-squid_ext:/data openpaas-squid_ext
```
```
WHITELIST_FILE=/data/example-acl-file.conf EGRESS_HTTP_PROXY_MODE=unit-test bash -x /data/containerfiles/openshift-entrypoint.sh
```

## openshift
```
cp /openshift-entrypoint.sh /tmp/openshift-entrypoint.sh
EGRESS_HTTP_PROXY_MODE=unit-test /tmp/openshift-entrypoint.sh
```
