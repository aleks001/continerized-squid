# Doku links

please read the metadata/README.md for creating

## Customer links

# Squid to run in openshift

```
Squid Cache: Version 3.3.8
```

# test the setup

```
oc run rhel-tools  --image=rhel7/rhel-tools
```

```
oc get po|egrep rhel-tools|egrep Running
oc rsh <THE_POD>

curl -v telnet://${OPENPAAS_SQUID_3_3_SERVICE_HOST}:${OPENPAAS_SQUID_3_3_SERVICE_PORT}

export http_proxy=http://${OPENPAAS_SQUID_3_3_SERVICE_HOST}:${OPENPAAS_SQUID_3_3_SERVICE_PORT}/
export https_proxy=http://${OPENPAAS_SQUID_3_3_SERVICE_HOST}:${OPENPAAS_SQUID_3_3_SERVICE_PORT}/

curl -o /dev/null -v -L --max-time 10 http://google.com/
curl -o /dev/null -v -L --max-time 10 https://google.com/
curl -o /dev/null -v -L https://google.com/

curl -v --max-time 10 http://twitter.com/
```
